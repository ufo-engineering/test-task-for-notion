import React, {Component} from 'react';
import FilterModule from './FilterModule'
import TableModule from "./TableModule";

function Home() {

  return (
      <React.Fragment>
        <FilterModule />
        <TableModule />
      </React.Fragment>
  );

}

export default Home;
