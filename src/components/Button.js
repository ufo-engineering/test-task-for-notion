import React, {Component} from 'react';


function Button(props) {
  console.log(props);
  return (
      <button
          className={props.className}
          onClick={props.handleClick}
          disabled={(props.disabled ? 'disabled' : '')}
      >
        {props.label}
      </button>
      );

}

export default Button;
