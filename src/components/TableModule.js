import React, {Component} from 'react';
import Button from "./Button";
import button from '../style/button.css'
import style from '../style/style.css'
import table from '../style/table.css'


class TableModule extends Component {
  viewDetails = () => {
    //view item API data
    alert('View item API data');
  }
  editData = () => {
    //edit item API data
    alert('Edit item API data');

  }
  removeItemData = () => {
    //remove item API data
    alert('Remove item API data');

  }

  render() {
    return (
        <div className={style.container}>
          <table className={table.table}>
            <tr>
              <td className={table.td}>1</td>
              <td className={table.td}>Value 1</td>
              <td className={table.td}>
                <Button className={`${button.btn} ${button.open}`} handleClick={this.viewDetails}/>
                <Button className={`${button.btn} ${button.edit}`} handleClick={this.editData}/>
                <Button className={`${button.btn} ${button.trash}`} handleClick={this.removeItemData}/>
              </td>
            </tr>
            <tr>
              <td className={table.td}>2</td>
              <td className={table.td}>Value 2</td>
              <td className={table.td}>
                <Button className={`${button.btn} ${button.open}`} handleClick={this.viewDetails}/>
                <Button className={`${button.btn} ${button.edit}`} handleClick={this.editData}/>
                <Button className={`${button.btn} ${button.trash}`} handleClick={this.removeItemData}/>
              </td>
            </tr>
          </table>
        </div>
    );
  }
}

export default TableModule;
