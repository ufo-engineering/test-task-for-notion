import React, {Component} from 'react';
import Button from "./Button";
import color from '../style/color.css';
import button from '../style/button.css'
import style from '../style/style.css'
import padding from '../style/padding.css'

class FilterModule extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDisabled: true
    }

    this.textInput = React.createRef();
    this.textDisplay = React.createRef();
  }

  enterText = () => {
    const value  = this.textInput.current.value;
    this.textDisplay.current.innerHTML = value;
  }

  resetFields = () => {
    this.textInput.current.value = '';
  }

  handleChange = () => {
    const value  = this.textInput.current.value;
    if (value != '') {
      this.setState({ isDisabled: false })
    } else {
      this.setState({ isDisabled: true })

    }
  }

  render() {
    const { isDisabled } = this.state;
    return (
        <div className={style.container}>
          <input type="text" ref={this.textInput} onChange={this.handleChange}/>
          <span ref={this.textDisplay}></span>
          <div className={style.btnWrap}>
            <Button
                className={`${color.primary} ${button.btn} ${padding.small}`}
                label={'Enter text'}
                handleClick={this.enterText}
            />
            <Button
                className={`${color.secondary} ${button.btn} ${padding.small} ${(isDisabled ? button.disabled : '')}`}
                label={'Reset'}
                handleClick={this.resetFields}
                disabled={isDisabled}

            />
          </div>

        </div>
    );
  }
}

export default FilterModule;
